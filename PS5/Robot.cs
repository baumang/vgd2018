using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using XELibrary;

namespace PS5
{
    public class Robot : Microsoft.Xna.Framework.DrawableGameComponent
    {
        private Model cube;
        private BasicEffect effect;

        public Camera Camera   { get; set; }
        public Matrix World    { get; set; }
        private Matrix Local   { get; set; }

        private float bodyRotationRate = 10.0f;
        private float handWaveRate = -20.0f;
        private float handWaveAngle = 0.0f;

        public Robot(Game game)
            : base(game)
        {

        }

        protected override void LoadContent()
        {
            cube = Game.Content.Load<Model>(@"Meshes\Cube");
            effect = new BasicEffect(Game.GraphicsDevice);
            effect.VertexColorEnabled = true;

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            Local = 
                Matrix.CreateRotationY(MathHelper.ToRadians(bodyRotationRate)
                * (float)gameTime.TotalGameTime.TotalSeconds);

            handWaveAngle += handWaveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (System.Math.Abs(handWaveAngle) > 40.0f)
            {
                handWaveRate = -handWaveRate;
                handWaveAngle = 40.0f * System.Math.Sign(handWaveAngle);
            }
                

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            // Head
            Matrix headTransform = 
                Matrix.CreateTranslation(new Vector3(0.0f, 1.25f, 0.0f));

            DrawCube(gameTime, headTransform);

            // Left Eye
            Matrix leftEyeTransform =
                Matrix.CreateScale(0.2f) *
                Matrix.CreateTranslation(new Vector3(0.3f, 0.2f, 0.5f)) *
                headTransform;

            DrawCube(gameTime, leftEyeTransform);

            // Right Eye
            Matrix rightEyeTransform =
                Matrix.CreateScale(0.2f) *
                Matrix.CreateTranslation(new Vector3(-0.3f, 0.2f, 0.5f)) *
                headTransform;

            DrawCube(gameTime, rightEyeTransform);

            // Body
            Matrix bodyTransform = 
                Matrix.CreateScale(new Vector3(1.5f, 1.5f, 1.5f));

            DrawCube(gameTime, bodyTransform);

            // Left Arm
            Matrix leftArmTransform = 
                Matrix.CreateScale(new Vector3(0.5f, 2.0f, 0.5f)) *
                Matrix.CreateTranslation(new Vector3(1.0f, -0.25f, 0.0f));

            DrawCube(gameTime, leftArmTransform);

            // Right Arm
            Matrix rightArmTransform =
                Matrix.CreateScale(new Vector3(0.5f, 2.0f, 0.5f)) *

                Matrix.CreateTranslation(new Vector3(0.0f, -0.75f, 0.0f)) *
                Matrix.CreateRotationX(MathHelper.ToRadians(handWaveAngle)) *
                Matrix.CreateTranslation(new Vector3(0.0f, 0.75f, 0.0f)) *

                Matrix.CreateTranslation(new Vector3(-1.0f, -0.25f, 0.0f));

            DrawCube(gameTime, rightArmTransform);

            // Left Leg
            Matrix leftLegTransform =
                Matrix.CreateScale(new Vector3(0.5f, 2.0f, 0.5f)) *
                Matrix.CreateTranslation(new Vector3(0.375f, -1.75f, 0.0f));

            DrawCube(gameTime, leftLegTransform);

            // Right Leg
            Matrix rightLegTransform = 
                Matrix.CreateScale(new Vector3(0.5f, 2.0f, 0.5f)) *
                Matrix.CreateTranslation(new Vector3(-0.375f, -1.75f, 0.0f));

            DrawCube(gameTime, rightLegTransform);

            base.Draw(gameTime);
        }


        private void DrawCube(GameTime gameTime, Matrix CubeTransform)
        {
            foreach (ModelMesh mesh in cube.Meshes)
            {
                foreach (ModelMeshPart meshPart in mesh.MeshParts)
                {
                    GraphicsDevice.SetVertexBuffer(meshPart.VertexBuffer, meshPart.VertexOffset);
                    GraphicsDevice.Indices = meshPart.IndexBuffer;

                    effect.World      = CubeTransform * Local * World;
                    effect.View       = Camera.View;
                    effect.Projection = Camera.Projection;
                    effect.CurrentTechnique.Passes[0].Apply();

                    GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, meshPart.PrimitiveCount);
                }
            }
        }
    }
}
