using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;

namespace PS5
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;

        Camera camera;
        InputHandler input;

        RasterizerState wireframeRaster, solidRaster;
        bool drawWireframe;

        Robot robot;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 800;

            input = new InputHandler(this);
            Components.Add(input);
            
            camera = new PerspectiveCamera(this);
            camera.CameraPosition = new Vector3(0.0f, 0.0f, 3.0f);
            Components.Add(camera);

            robot = new Robot(this);
            Components.Add(robot);

            wireframeRaster = new RasterizerState();
            wireframeRaster.FillMode = FillMode.WireFrame;

            solidRaster = new RasterizerState();
            solidRaster.FillMode = FillMode.Solid;

            drawWireframe = false;
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (input.KeyboardHandler.WasKeyPressed(Keys.W))
                drawWireframe = !drawWireframe;
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            if (drawWireframe)
                GraphicsDevice.RasterizerState = wireframeRaster;
            else
                GraphicsDevice.RasterizerState = solidRaster;
            
            Matrix world = Matrix.Identity;

            world = 
                Matrix.CreateScale(0.3f) * // Scale everything to a smaller size
                world;

            robot.Camera = camera;
            robot.World  = world;

            base.Draw(gameTime);
        }
    }
}
