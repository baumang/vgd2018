using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics.DebugView;

namespace PS4
{
    public class HelloPhysics : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        
        // Physics
        private World world;
        private float dt = 0.001f;
        private const float G = 9.81f;
        private Vector2 gravity = new Vector2(0.0f, -G);

        private bool paused = false;

        private List<Fixture> circles = new List<Fixture>();
        private List<Fixture> platforms = new List<Fixture>();

        private Rope rope;
        private Vector2 ropeCreateStart, ropeCreateEnd;
        private bool isCreatingRope = false;
        
        Border border;

        // Camera
        private Camera2D camera;

        // Input
        private InputHandler input;

        // Debug view
        DebugViewXNA debugView;
        
        public HelloPhysics()
        {
            graphics = new GraphicsDeviceManager(this);

            graphics.SynchronizeWithVerticalRetrace = false;
            graphics.PreferMultiSampling = true;
            IsFixedTimeStep = true;
            
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
                        
            Content.RootDirectory = "Content";

            input = new InputHandler(this);
            Components.Add(input);

            world = new World(gravity);
            debugView = new DebugViewXNA(world);

            rope = new Rope(this, world); 
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            camera = new Camera2D(GraphicsDevice);
#if true
            Fixture c1;
            c1 = FixtureFactory.AttachCircle(3.0f, 50.0f, new Body(world, new Vector2(-8.0f, 3.0f)));
            c1.Body.BodyType = BodyType.Dynamic;
            c1.CollisionCategories = Category.Cat1;
            c1.Restitution = 0.8f;
            circles.Add(c1);
            
            Fixture p1;
            p1 = FixtureFactory.AttachRectangle(24.0f, 2.0f, 1.0f, new Vector2(-8.0f, 0.0f), new Body(world));
            p1.Body.BodyType = BodyType.Static;
            p1.CollisionCategories = Category.Cat2;
            platforms.Add(p1);
#endif

            Vector2 gameWorld =
                Camera2D.ConvertScreenToWorld(new Vector2(camera.ScreenWidth,
                                                        camera.ScreenHeight));
            border = new Border(world, gameWorld.X, gameWorld.Y, 0.1f);
            
            debugView.LoadContent(GraphicsDevice, Content);
            debugView.DefaultShapeColor = Color.White;
            debugView.SleepingShapeColor = Color.White;

            debugView.RemoveFlags(DebugViewFlags.Joint);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            HandleKeyboard(gameTime);
            HandleMouse(gameTime);

            // Advance the physics simulation
            if (!paused)
                world.Step(Math.Min((float)gameTime.ElapsedGameTime.TotalMilliseconds * dt, (1f / 30f)));
            
           base.Update(gameTime);
        }

        private void HandleMouse(GameTime gameTime)
        {
            if (input.MouseHandler.IsHoldingLeftButton())
            {
                if (input.MouseHandler.MouseState.X < 0 ||
                    input.MouseHandler.MouseState.X > GraphicsDevice.Viewport.Width ||
                    input.MouseHandler.MouseState.Y < 0 ||
                    input.MouseHandler.MouseState.Y > GraphicsDevice.Viewport.Height)
                    return;

                if (!isCreatingRope)
                {
                    isCreatingRope = true;
                    ropeCreateStart = Camera2D.ConvertScreenToWorld(new Vector2(input.MouseHandler.MouseState.X, input.MouseHandler.MouseState.Y));
                }
                ropeCreateEnd = Camera2D.ConvertScreenToWorld(new Vector2(input.MouseHandler.MouseState.X, input.MouseHandler.MouseState.Y));
            }
            if (input.MouseHandler.WasLeftButtonClicked())
            {
                if (isCreatingRope)
                {
                    isCreatingRope = false;
                    bool ropeSuccessful = rope.CreateRope(ropeCreateStart, ropeCreateEnd);
                    if (ropeSuccessful)
                    {
                        Vector2 initialImpulse = (ropeCreateStart - ropeCreateEnd);
                        initialImpulse.Normalize();
                        initialImpulse *= 500.0f;
                        rope.ApplyLinearImpulse(ref initialImpulse);
                    }
                }
            }
        }

        private void HandleKeyboard(GameTime gameTime)
        {
            if (!paused && input.KeyboardHandler.WasKeyPressed(Keys.Space))
            {
                Vector2 impulse = new Vector2(0.5f, 0.5f);
                circles[0].Body.ApplyLinearImpulse(ref impulse);
            }

            if (paused && input.KeyboardHandler.IsHoldingKey(Keys.Left))
                world.Step(Math.Min((float)gameTime.ElapsedGameTime.TotalMilliseconds * dt, (1f / 30f)));

            if (input.KeyboardHandler.WasKeyPressed(Keys.F1))
                ToggleFlag(DebugViewFlags.Shape);
            if (input.KeyboardHandler.WasKeyPressed(Keys.F2))
                ToggleFlag(DebugViewFlags.DebugPanel);
            if (input.KeyboardHandler.WasKeyPressed(Keys.F3))
                ToggleFlag(DebugViewFlags.PerformanceGraph);
            if (input.KeyboardHandler.WasKeyPressed(Keys.F4))
                ToggleFlag(DebugViewFlags.AABB);
            if (input.KeyboardHandler.WasKeyPressed(Keys.F5))
                ToggleFlag(DebugViewFlags.CenterOfMass);
            if (input.KeyboardHandler.WasKeyPressed(Keys.F6))
                ToggleFlag(DebugViewFlags.Joint);
            if (input.KeyboardHandler.WasKeyPressed(Keys.F7))
            {
                ToggleFlag(DebugViewFlags.ContactPoints);
                ToggleFlag(DebugViewFlags.ContactNormals);
            }
            if (input.KeyboardHandler.WasKeyPressed(Keys.F8))
                ToggleFlag(DebugViewFlags.PolygonPoints);
            if (input.KeyboardHandler.WasKeyPressed(Keys.P))
                paused = !paused;
            if (input.KeyboardHandler.WasKeyPressed(Keys.G))
            {
                if (world.Gravity.Equals(Vector2.Zero))
                    world.Gravity = new Vector2(0.0f, -G);
                else
                    world.Gravity = Vector2.Zero;
            }
        }

        private void ToggleFlag(DebugViewFlags flag)
        {
            if ((debugView.Flags & flag) == flag)
                debugView.RemoveFlags(flag);
            else
                debugView.AppendFlags(flag);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            if (isCreatingRope)
            {
                debugView.BeginCustomDraw(ref Camera2D.Projection, ref Camera2D.View);
                debugView.DrawSegment(ropeCreateStart, ropeCreateEnd, Color.White);
                debugView.EndCustomDraw();
            }

            debugView.RenderDebugData(ref Camera2D.Projection, ref Camera2D.View);

            base.Draw(gameTime);
        }
    }
}
