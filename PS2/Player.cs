using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;

namespace PS2
{
    public class Player : Microsoft.Xna.Framework.DrawableGameComponent
    {
        ICelAnimationManager celAnimationManager;
        IInputHandler inputHandler;

        SpriteBatch spriteBatch;

        private Vector2 position = new Vector2(20, 520);
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        enum Direction { Left, Right };

        private Direction direction = Direction.Right;
        private float moveRate = 150.0f;

        public Player(Game game)
            : base(game)
        {
            celAnimationManager = (ICelAnimationManager)game.Services.GetService(typeof(ICelAnimationManager));
            inputHandler = (IInputHandler)game.Services.GetService(typeof(IInputHandler));
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }

        protected override void LoadContent()
        {
            CelCount celCount = new CelCount(7, 1);
            celAnimationManager.AddAnimation("sonicRun", "sonic", celCount, 10);
            celAnimationManager.ToggleAnimation("sonicRun");
        }

        public override void Update(GameTime gameTime)
        {
            if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Right))
            {
                celAnimationManager.ResumeAnimation("sonicRun");
                direction = Direction.Right;
                position.X += (moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }
            else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Left))
            {
                celAnimationManager.ResumeAnimation("sonicRun");
                direction = Direction.Left;
                position.X -= (moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }
            else
                celAnimationManager.PauseAnimation("sonicRun");

            int celWidth = celAnimationManager.GetAnimationFrameWidth("sonicRun");

            if (position.X > (Game.GraphicsDevice.Viewport.Width - celWidth))
                position.X =  (Game.GraphicsDevice.Viewport.Width - celWidth);
            if (position.X < 0)
                position.X = 0;

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            celAnimationManager.Draw(gameTime, "sonicRun", spriteBatch, position, direction == Direction.Right ? SpriteEffects.None : SpriteEffects.FlipHorizontally);
            spriteBatch.End();
        }
    }
}
