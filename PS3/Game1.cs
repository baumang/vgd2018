using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;

namespace PS3
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        CelAnimationManager celAnimationManager;
        ScrollingBackgroundManager scrollingBackgroundManager;
        
        private InputHandler inputHandler;

        private Player player;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1000;
            graphics.PreferredBackBufferHeight = 600;

            Content.RootDirectory = "Content";

            inputHandler = new InputHandler(this);
            Components.Add(inputHandler);

            celAnimationManager = new CelAnimationManager(this, "Textures\\");
            Components.Add(celAnimationManager);

            scrollingBackgroundManager = new ScrollingBackgroundManager(this, "Textures\\");
            Components.Add(scrollingBackgroundManager);

            player = new Player(this);
            Components.Add(player);
        }

        protected override void Initialize()
        {

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            player.Load(spriteBatch);

            scrollingBackgroundManager.AddBackground("caveBG", "cave_bg", new Vector2(0, 0), new Rectangle(0, 0, 1600, 600), 10, 0.5f, Color.White);
            scrollingBackgroundManager.AddBackground("caveWalk", "cave_walk", new Vector2(0, 550), new Rectangle(0, 0, 512, 60), 100, 0.1f, Color.White);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Right) && player.Position.X >= (graphics.GraphicsDevice.Viewport.Width / 2.0f))
                scrollingBackgroundManager.ScrollRate = -2.0f;
            else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Left) && player.Position.X <= 0.0f)
                scrollingBackgroundManager.ScrollRate = 2.0f;
            else
                scrollingBackgroundManager.ScrollRate = 0.0f;

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.BackToFront);

            scrollingBackgroundManager.Draw("caveBG", spriteBatch);
            scrollingBackgroundManager.Draw("caveWalk", spriteBatch);

            base.Draw(gameTime);

            spriteBatch.End();
        }
    }
}
