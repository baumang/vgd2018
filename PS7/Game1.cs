using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using XELibrary;

namespace PS7
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager graphics;

        public SpriteBatch SpriteBatch { get; private set; }

        CelAnimationManager celAnimationManager;
        ScrollingBackgroundManager scrollingBackgroundManager;
        InputHandler inputHandler;
        SoundManager soundManager;
        GameStateManager stateManager;

        public ITitleIntroState TitleIntroState;
        public IStartMenuState StartMenuState;
        public IPlayingState PlayingState;
        public IPausedState PausedState;
        public IOptionsMenuState OptionsMenuState;

        public bool EnableSoundFx { get; set; }
        public bool EnableMusic { get; set; }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1000;
            graphics.PreferredBackBufferHeight = 600;

            Content.RootDirectory = "Content";

            inputHandler = new InputHandler(this);
            Components.Add(inputHandler);

            celAnimationManager = new CelAnimationManager(this, "Textures\\");
            Components.Add(celAnimationManager);

            scrollingBackgroundManager = new ScrollingBackgroundManager(this, "Textures\\");
            Components.Add(scrollingBackgroundManager);

            soundManager = new SoundManager(this);
            Components.Add(soundManager);

            stateManager = new GameStateManager(this);
            Components.Add(stateManager);

            TitleIntroState = new TitleIntroState(this);
            StartMenuState = new StartMenuState(this);
            PlayingState = new PlayingState(this);
            PausedState = new PausedState(this);
            OptionsMenuState = new OptionsMenuState(this);
            
            EnableSoundFx = true;
            EnableMusic = true;
        }

        protected override void BeginRun()
        {
            stateManager.ChangeState(TitleIntroState.Value);

            base.BeginRun();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            // Load sounds
            string musicPath = @"Sounds\Music";
            string fxPath    = @"Sounds\SoundFX\";

            soundManager.LoadContent(musicPath, fxPath);
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }


        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            SpriteBatch.Begin();
            base.Draw(gameTime);
            SpriteBatch.End();
        }
    }
}
