using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using XELibrary;

namespace PS6
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;

        Camera camera;
        InputHandler input;

        enum DrawableModels
        {
            Flag,
            Earth
        };
        DrawableModels selectedModel;

        ModelMesh flagMesh;
        int flagSegments = 100;
        Texture2D flagTexture;
        float flagRotationRate = 1.0f; // 1 degree per second
        float flagRotation = 0.0f; // In radians
        float flagRotationFactorRate = 0.1f;
        float flagRotationFactor = 0.0f;

        Model earth;
        float earthRotationRate = 1.0f; // 1 degree per second
        float earthRotation = 0.0f; // In radians
        Vector3 ambientMaterial;
        Vector3 diffuseMaterial;
        Vector3 specularMaterial;

        BasicEffect flagEffect;
        BasicEffect earthEffect;

        Vector3 ambientLight;
        Vector3 diffuseLight;
        Vector3 specularLight;
        Vector3 lightPosition;

        RasterizerState wireframeRaster, solidRaster;

        // States
        bool drawWireframe;
        bool enableTextures;
        bool enableLighting;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 800;

            input = new InputHandler(this);
            Components.Add(input);
            
            camera = new PerspectiveCamera(this);
            camera.CameraPosition = new Vector3(0.0f, 0.0f, 2.0f);
            Components.Add(camera);

            wireframeRaster = new RasterizerState();
            wireframeRaster.FillMode = FillMode.WireFrame;

            solidRaster = new RasterizerState();
            solidRaster.FillMode = FillMode.Solid;

            drawWireframe = false;
            enableLighting = true;
            enableTextures = true;

            selectedModel = DrawableModels.Flag;
        }

        protected override void LoadContent()
        {
            GenerateFlag();
            flagTexture = Content.Load<Texture2D>(@"Textures\Homer2");
            flagEffect = new BasicEffect(GraphicsDevice);
            flagEffect.Texture = flagTexture;

            earth = Content.Load<Model>(@"Models\earth");
            earthEffect = earth.Meshes[0].MeshParts[0].Effect as BasicEffect;

            ambientMaterial  = new Vector3(0.30f, 0.30f, 0.30f);
            diffuseMaterial  = new Vector3(0.65f, 0.65f, 0.65f);
            specularMaterial = new Vector3(0.50f, 0.50f, 0.50f);

            ambientLight  = new Vector3(1.00f, 1.00f, 1.00f);
            diffuseLight  = new Vector3(1.00f, 1.00f, 1.00f);
            specularLight = new Vector3(1.00f, 1.00f, 1.00f);
            lightPosition = new Vector3(-1.0f, -0.2f, -1.0f);

            base.LoadContent();
        }

        private void GenerateFlag()
        {
            List<ModelMeshPart> flagMeshParts = new List<ModelMeshPart>();

            for (int i = 0; i < flagSegments; ++i)
            {
                VertexPositionNormalTexture[] flagVertices = new VertexPositionNormalTexture[4];
                short[] flagIndices = new short[6];

                flagVertices[0] = new VertexPositionNormalTexture(
                    new Vector3((i / (float)flagSegments) - 0.5f, +0.5f, 0.0f),
                    Vector3.UnitZ,
                    new Vector2((i / (float)flagSegments), 0.0f));
                flagVertices[1] = new VertexPositionNormalTexture(
                    new Vector3((i / (float)flagSegments) - 0.5f, -0.5f, 0.0f),
                    Vector3.UnitZ,
                    new Vector2((i / (float)flagSegments), 1.0f));
                flagVertices[2] = new VertexPositionNormalTexture(
                    new Vector3((i / (float)flagSegments) - 0.5f + (1.0f / flagSegments), -0.5f, 0.0f),
                    Vector3.UnitZ,
                    new Vector2((i + 1) / (float)flagSegments, 1.0f));
                flagVertices[3] = new VertexPositionNormalTexture(
                    new Vector3((i / (float)flagSegments) - 0.5f + (1.0f / flagSegments), +0.5f, 0.0f),
                    Vector3.UnitZ,
                    new Vector2((i + 1) / (float)flagSegments, 0.0f));

                flagIndices[0] = 2;
                flagIndices[2] = 0;
                flagIndices[1] = 1;
                flagIndices[3] = 0;
                flagIndices[5] = 2;
                flagIndices[4] = 3;
                
                ModelMeshPart flagMeshPart = new ModelMeshPart();
                flagMeshPart.VertexBuffer = new VertexBuffer(GraphicsDevice, VertexPositionNormalTexture.VertexDeclaration, flagVertices.Length, BufferUsage.WriteOnly);
                flagMeshPart.VertexBuffer.SetData<VertexPositionNormalTexture>(flagVertices);
                flagMeshPart.IndexBuffer = new IndexBuffer(GraphicsDevice, IndexElementSize.SixteenBits, flagIndices.Length, BufferUsage.WriteOnly);
                flagMeshPart.IndexBuffer.SetData<short>(flagIndices);

                flagMeshParts.Add(flagMeshPart);
            }

            flagMesh = new ModelMesh(GraphicsDevice, flagMeshParts);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            HandleInput(gameTime);

            switch (selectedModel)
            {
                case DrawableModels.Flag:
                    flagRotation += MathHelper.ToRadians(flagRotationRate) * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (flagRotation > MathHelper.TwoPi)
                    {
                        flagRotation -= MathHelper.TwoPi;
                    }

                    flagRotationFactor += flagRotationFactorRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (flagRotationFactor > 1.0f || flagRotationFactor < 0.0f)
                    {
                        flagRotationFactorRate = -flagRotationFactorRate;
                    }
                    break;
                case DrawableModels.Earth:
                    earthRotation += MathHelper.ToRadians(earthRotationRate) * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (earthRotation > MathHelper.TwoPi)
                    {
                        earthRotation -= MathHelper.TwoPi;
                    }
                    break;
            }

            base.Update(gameTime);
        }

        private void HandleInput(GameTime gameTime)
        {
            if (input.KeyboardHandler.WasKeyPressed(Keys.W))
                drawWireframe = !drawWireframe;
            if (input.KeyboardHandler.WasKeyPressed(Keys.L))
                enableLighting = !enableLighting;
            if (input.KeyboardHandler.WasKeyPressed(Keys.T))
                enableTextures = !enableTextures;
            if (input.KeyboardHandler.WasKeyPressed(Keys.F1))
                selectedModel = DrawableModels.Flag;
            if (input.KeyboardHandler.WasKeyPressed(Keys.F2))
                selectedModel = DrawableModels.Earth;

        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            
            if (drawWireframe)
                GraphicsDevice.RasterizerState = wireframeRaster;
            else
                GraphicsDevice.RasterizerState = solidRaster;

            BasicEffect effectToUpdate;

            switch (selectedModel)
            {
                case DrawableModels.Flag:
                    effectToUpdate = flagEffect;
                    camera.CameraPosition = new Vector3(0.0f, 0.0f, 2.0f);
                    break;
                case DrawableModels.Earth:
                default:
                    camera.CameraPosition = new Vector3(0.0f, 0.0f, 10.0f);
                    effectToUpdate = earthEffect;
                    break;
            }

            effectToUpdate.AmbientLightColor               = ambientMaterial;
            effectToUpdate.DiffuseColor                    = diffuseMaterial;
            effectToUpdate.SpecularColor                   = specularMaterial;
            effectToUpdate.DirectionalLight0.DiffuseColor  = diffuseLight;
            effectToUpdate.DirectionalLight0.SpecularColor = specularLight;
            effectToUpdate.DirectionalLight0.Direction     = lightPosition;
            effectToUpdate.DirectionalLight0.Enabled       = true;
            effectToUpdate.TextureEnabled                  = enableTextures;
            effectToUpdate.LightingEnabled                 = enableLighting;

            Matrix world = Matrix.Identity;

            switch (selectedModel)
            {
                case DrawableModels.Flag:
                    DrawFlag(gameTime, world);
                    break;
                case DrawableModels.Earth:
                default:
                    DrawEarth(gameTime, world);
                    break;
            }

            base.Draw(gameTime);
        }

        private void DrawFlag(GameTime gameTime, Matrix world)
        {
            int segmentIndex = 0;
            float segmentAngle = MathHelper.TwoPi / flagMesh.MeshParts.Count;
            float segmentWidth = (1.0f / flagSegments);

            Matrix segmentTransform =
                
                Matrix.CreateRotationY(-MathHelper.PiOver4) *
                world;

            foreach (ModelMeshPart quadMesh in flagMesh.MeshParts)
            {
                float segmentX = -0.5f + (segmentIndex * segmentWidth);
                segmentTransform =
                    Matrix.CreateTranslation(new Vector3(-segmentX, 0.0f, 0.0f)) *
                    Matrix.CreateRotationY(segmentAngle * flagRotationFactor) *
                    Matrix.CreateTranslation(new Vector3(+segmentX, 0.0f, 0.0f)) *
                    segmentTransform;

                flagEffect.World = 
                    segmentTransform;

                flagEffect.View       = camera.View;
                flagEffect.Projection = camera.Projection;
                flagEffect.Texture    = flagTexture;
                GraphicsDevice.SetVertexBuffer(quadMesh.VertexBuffer);
                GraphicsDevice.Indices = quadMesh.IndexBuffer;
                flagEffect.CurrentTechnique.Passes[0].Apply();

                GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, quadMesh.IndexBuffer.IndexCount);

                segmentIndex++;
            }
        }

        private void DrawEarth(GameTime gameTime, Matrix world)
        {
            Matrix earthTransform =
                Matrix.CreateRotationY(earthRotation) *
                world;
            
            earth.Draw(earthTransform, camera.View, camera.Projection);
        }
    }
}
